# ReactJS - A learning journey

## 1) ReactJS Basics
See the code at: `tutorial-app-ts-version`
* Learn how to create a basic app using `npx create-react-app`
* Refactoring the existing app to use strongly typed language Typescript

## 2) ReactJS with Docker
See the code at: `dockerize-react-app`
* Apply Docker knowledge to the tutorial app
* Learn how to do different configurations for Development and Production mode

## 3) Test-Driven-Development ReactJS App
See the code at: `tdd-react-app`
* Create a simple Message List app in Typescript
* Learn how to use the Cypress end-to-end test runner
* Improve the existing unit test files
