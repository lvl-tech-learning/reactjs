# This app was created using the tutorial in
[Dockerizing a React App](https://mherman.org/blog/dockerizing-a-react-app/).

# Dependencies
* [Git](https://git-scm.com/downloads)
* [Create React App](https://github.com/facebook/create-react-app)
* [Docker](https://docs.docker.com/engine/install/)
* [Docker Compose](https://docs.docker.com/compose/install/)

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Available Scripts

In the project directory, you can run:

### Build and tag the Docker image
* Dev mode
```
docker build -t sample:dev .
```
* Production mode
```
docker build -f Dockerfile.prod -t sample:prod .
```

### Run the container 
* Production mode
```
docker run -it --rm -p 1337:80 sample:prod
```
    * You can see the image running on
    `{machine_name}:1337`

* Dev mode - In interactive mode and hot reloading :
```
docker run \
    -it \
    --rm \
    -v ${PWD}:/app \
    -v /app/node_modules \
    -p 3001:3000 \
    -e CHOKIDAR_USEPOLLING=true \
    sample:dev
```
    * You can see the image running on
    `{machine_name}:3001`

### Run the container with compose
* Dev mode - In interactive mode and hot reloading:
```
docker-compose up -d --build
```
    * You can see the image running on
    `{machine_name}:3001`
* Prod
```
docker-compose -f docker-compose.prod.yml up -d --build
```