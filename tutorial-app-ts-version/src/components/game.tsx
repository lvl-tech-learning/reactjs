import React from "react";
import { calculateWinner } from "../functions/calculate-winner";
import Board from "./board";

interface SquaresList { squares: Array<string> };
interface GameState { history: Array<SquaresList>; xIsNext: boolean; stepNumber: number };
interface GameProps { };

/**
 * Game stores the history of moves in a Board 
 */
export default class Game extends React.Component<GameProps, GameState> {

    constructor(props: GameProps) {
        super(props);
        this.state = {
            history: [{ squares: Array(9).fill('') }],
            xIsNext: true,
            stepNumber: 0
        };
    }

    jumpTo(step: number) {
        this.setState({
            stepNumber: step,
            xIsNext: (step % 2) === 0,
        })
    }


    handleClick(i: number) {
        const history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[history.length - 1];
        // Immutability Is Important - slice is used here to create a copy of the existing state

        const squaresCpy = current.squares.slice();
        if (calculateWinner(squaresCpy) || squaresCpy[i]) {
            return;
        }
        squaresCpy[i] = this.state.xIsNext ? 'X' : 'O';

        // concat() method doesn’t mutate the original array
        this.setState({
            history: history.concat([{ squares: squaresCpy, }]),
            xIsNext: !this.state.xIsNext,
            stepNumber: history.length,
        });
    }

    render() {
        const history = this.state.history;
        const current = history[this.state.stepNumber];
        const winner = calculateWinner(current.squares);
        const whoIsNext = this.state.xIsNext ? 'X' : 'O';
        let status = winner ? `Winner: ${winner}` : `Next player: ${whoIsNext}`;

        const moves = history.map((step, move) => {
            const desc = move ? `Go to move #${move}` : 'Go to game start';

            /**
             * key is a special and reserved property in React 
             * When an element is created, React extracts the key property and stores the key directly on the returned element
             * It’s strongly recommended that you assign proper keys whenever you build dynamic lists
             */
            return (<li key={move}>
                <button onClick={() => this.jumpTo(move)}>{desc}</button>
            </li>
            );
        });

        return (
            <div className="game">
                <div className="game-board">
                    <Board
                        squares={current.squares}
                        onClick={(i: number) => this.handleClick(i)}
                    />
                </div>
                <div className="game-info">
                    <div>{status}</div>
                    <ol>{moves}</ol>
                </div>
            </div>
        );
    }
}
