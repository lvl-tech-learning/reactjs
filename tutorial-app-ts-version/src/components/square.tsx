
import React from "react";

interface SquareProps { value: string; onClick: () => void };
/**
 * Square function component renders a single <button>
 * Function components are a simpler way to write components 
 * that only contain a render method and don’t have their own state
 */
const Square: React.FunctionComponent<SquareProps> = (props: SquareProps) => {
    return (
        <button className="square" onClick={props.onClick}>
            {props.value}
        </button>
    );
};

export default Square;
