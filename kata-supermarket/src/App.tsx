import './App.css';
import { InventoryProps } from './interfaces/InventoryProps';
import Inventory from './inventory/Inventory';

const App: React.FC = () => {
  let testInventory: InventoryProps = {
    itemList: [
      { name: 'beans', price: 0.65 },
      { name: 'rice', price: 1.00 },
    ]
  };

  return (
    <div data-testid="main-app">
      <Inventory itemList={testInventory.itemList} />

      <button data-testid="add-to-basket" >

      </button>

      <p data-testid="basket-total">0.65</p>
    </div>
  );
};
export default App;
