import { ProductProps } from "../interfaces/ProductProp";

const Product: React.FC<ProductProps> = (props: ProductProps) => {
    return (
        <tr data-testid="product-element">
            <td data-testid="item-name">{props.name}</td>
            <td data-testid="item-price">{props.price}</td>
        </tr>
    );
};
export default Product;
