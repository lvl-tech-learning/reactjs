import { RenderResult, render } from "@testing-library/react";
import React from "react";
import { ProductProps } from "../interfaces/ProductProp";
import Product from "./Product";

describe('Product', () => {
    let renderedUI: RenderResult;
    let testProduct: ProductProps = { name: 'beans', price: 0.65 };

    beforeEach(() => {
        renderedUI = render(<Product name={testProduct.name} price={testProduct.price} />);
    });

    function ValidateItemName(expected: string) {
        const renderedElement = renderedUI.getByTestId('item-name');

        expect(renderedElement.textContent).toEqual(expected);
    }

    function ValidateItemPrice(expected: number) {
        const renderedElement = renderedUI.getByTestId('item-price');

        expect(renderedElement.textContent).toEqual(expected.toString());
    }

    it('displays the supermarket inventory list', () => {
        ValidateItemName(testProduct.name);
        ValidateItemPrice(testProduct.price);
    });
});