import React from "react";
import { InventoryProps } from "../interfaces/InventoryProps";
import { ProductProps } from "../interfaces/ProductProp";
import Product from "../product/Product";

const Inventory: React.FC<InventoryProps> = (props: InventoryProps) => {
    return (
        <table data-testid="inventory-element">
            <thead>
                <tr data-testid="inventory-header">
                    <td>Product Name</td>
                    <td>Price</td>
                </tr>
            </thead>
            <tbody>
                {props.itemList.map((item: ProductProps) =>
                    <Product name={item.name} price={item.price} />
                )}
            </tbody>
        </table>
    );
};
export default Inventory;
