import { RenderResult, render } from "@testing-library/react";
import React from "react";
import { InventoryProps } from "../interfaces/InventoryProps";
import Inventory from "./Inventory";

describe('App', () => {
    let renderedUI: RenderResult;
    let testInventory: InventoryProps = {
        itemList: [
            { name: 'beans', price: 0.65 },
            { name: 'rice', price: 1.00 },
        ]
    }

    beforeEach(() => {
        renderedUI = render(<Inventory itemList={testInventory.itemList} />);
    });

    function ValidateItemName(elementIndex: number, expected: string) {
        const renderedElement = renderedUI.getAllByTestId('item-name');

        expect(renderedElement[elementIndex].textContent).toEqual(expected);
    }

    function ValidateItemPrice(elementIndex: number, expected: number) {
        const renderedElement = renderedUI.getAllByTestId('item-price');

        expect(renderedElement[elementIndex].textContent).toEqual(expected.toString());
    }

    it('displays the inventory header', () => {
        const renderedElement = renderedUI.getByTestId('inventory-header');

        expect(renderedElement.textContent).toContain('Product Name');
        expect(renderedElement.textContent).toContain('Price');
    });

    it('displays the supermarket inventory list', () => {
        const itemList = testInventory.itemList;
        for (let index = 0; index < itemList.length; index++) {
            ValidateItemName(index, itemList[index].name);
            ValidateItemPrice(index, itemList[index].price);
        }
    });
});