import { ProductProps } from "./ProductProp";

export interface InventoryProps {
    itemList: ProductProps[];
};