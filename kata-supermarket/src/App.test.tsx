import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import App from './App';

describe('App', () => {
  let renderedUI: RenderResult;
  beforeEach(() => {
    renderedUI = render(<App />);
  });

  it('renders the inventory element', () => {
    expect(renderedUI.getByTestId('inventory-element')).toBeDefined();
  });

  it('allows the user to add item to basket', () => {
    const addToBasketBtn = renderedUI.getAllByTestId('add-to-basket')[0];
    addToBasketBtn.click();

    expect(renderedUI.getByTestId('basket-total').textContent).toContain('0.65');
  });
});

