import React from 'react';
interface MessageProps {
    data: string[];
};

const MessageList: React.FC<MessageProps> = (props: MessageProps) => {
    return (
        <ul>
            {props.data.map((message: string) => <li key={message}>{message}</li>)}
        </ul>
    );
};

export default MessageList;
