import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import App from './App';

describe('App', () => {
  let renderedUI: RenderResult;
  beforeEach(() => {
    renderedUI = render(<App />);
  });

  it('renders the main page', () => {
    expect(renderedUI.getByTestId('mainApp')).toBeDefined();
  });
});