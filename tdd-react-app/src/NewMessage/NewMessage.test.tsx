import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import NewMessage from './NewMessage';

describe('NewMessage', () => {
  describe('when Send button is clicked', () => {
    let sendHandler: jest.Mock<any, any>;
    let renderedUI: RenderResult<typeof import("@testing-library/dom/types/queries")>;

    beforeEach(async () => {
      sendHandler = jest.fn().mockName('sendHandler');
      renderedUI = render(<NewMessage onSend={sendHandler} />);

      await userEvent.type(
        renderedUI.getByTestId('messageText'),
        'New message',
      );

      userEvent.click(renderedUI.getByTestId('sendButton'));
    });

    it('should clear the message field ', () => {
      const messageField: HTMLInputElement = renderedUI.getByTestId('messageText') as HTMLInputElement;
      expect(messageField.value).toEqual('');
    });

    it('should call the Send handler function ', () => {
      expect(sendHandler).toHaveBeenCalledWith('New message');
    });
  });
});