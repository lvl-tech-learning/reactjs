import React, { useState } from 'react';


interface NewMessageProps {
    onSend: (newMessage: string) => void;
};

const NewMessage: React.FC<NewMessageProps> = (props: NewMessageProps) => {
    const [inputText, setInputText] = useState('');

    const handleTextChange = (event: { target: { value: React.SetStateAction<string>; }; }) => {
        setInputText(event.target.value);
    };

    const handleSend = () => {
        props.onSend(inputText);
        setInputText('');
    };

    return (
        <div>
            <input
                type="text"
                data-testid="messageText"
                value={inputText}
                onChange={handleTextChange}>
            </input>

            <button
                data-testid="sendButton"
                onClick={handleSend}>
                Send
            </button>
        </div>
    );
};

export default NewMessage;