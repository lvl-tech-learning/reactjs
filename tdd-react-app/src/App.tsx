import React, { useState } from 'react';
import './App.css';
import MessageList from './MessageList/MessageList';
import NewMessage from './NewMessage/NewMessage';

const App: React.FC = () => {
  const [messages, setMessages] = useState<string[]>([]);
  const handleSend = (newMessage: string) => {
    setMessages([newMessage, ...messages]);
  };

  return (
    <div data-testid="mainApp">
      <NewMessage onSend={handleSend} />
      <MessageList data={messages} />
    </div>
  );
};
export default App;
